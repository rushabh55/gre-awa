﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="AWACloud_______" generation="1" functional="0" release="0" Id="f647a70d-606e-4271-b7b6-f45a40697036" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="AWACloud_______Group" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="AWA:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/AWACloud_______/AWACloud_______Group/LB:AWA:Endpoint1" />
          </inToChannel>
        </inPort>
        <inPort name="AWA:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" protocol="tcp">
          <inToChannel>
            <lBChannelMoniker name="/AWACloud_______/AWACloud_______Group/LB:AWA:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="AWA:CloudToolsDiagnosticAgentVersion" defaultValue="">
          <maps>
            <mapMoniker name="/AWACloud_______/AWACloud_______Group/MapAWA:CloudToolsDiagnosticAgentVersion" />
          </maps>
        </aCS>
        <aCS name="AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" defaultValue="">
          <maps>
            <mapMoniker name="/AWACloud_______/AWACloud_______Group/MapAWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" />
          </maps>
        </aCS>
        <aCS name="AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" defaultValue="">
          <maps>
            <mapMoniker name="/AWACloud_______/AWACloud_______Group/MapAWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" />
          </maps>
        </aCS>
        <aCS name="AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" defaultValue="">
          <maps>
            <mapMoniker name="/AWACloud_______/AWACloud_______Group/MapAWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" />
          </maps>
        </aCS>
        <aCS name="AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" defaultValue="">
          <maps>
            <mapMoniker name="/AWACloud_______/AWACloud_______Group/MapAWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" />
          </maps>
        </aCS>
        <aCS name="AWA:Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" defaultValue="">
          <maps>
            <mapMoniker name="/AWACloud_______/AWACloud_______Group/MapAWA:Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" />
          </maps>
        </aCS>
        <aCS name="AWA:Profiling.ProfilingConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/AWACloud_______/AWACloud_______Group/MapAWA:Profiling.ProfilingConnectionString" />
          </maps>
        </aCS>
        <aCS name="AWAInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/AWACloud_______/AWACloud_______Group/MapAWAInstances" />
          </maps>
        </aCS>
        <aCS name="Certificate|AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" defaultValue="">
          <maps>
            <mapMoniker name="/AWACloud_______/AWACloud_______Group/MapCertificate|AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:AWA:Endpoint1">
          <toPorts>
            <inPortMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Endpoint1" />
          </toPorts>
        </lBChannel>
        <lBChannel name="LB:AWA:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput">
          <toPorts>
            <inPortMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" />
          </toPorts>
        </lBChannel>
        <sFSwitchChannel name="SW:AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp">
          <toPorts>
            <inPortMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" />
          </toPorts>
        </sFSwitchChannel>
      </channels>
      <maps>
        <map name="MapAWA:CloudToolsDiagnosticAgentVersion" kind="Identity">
          <setting>
            <aCSMoniker name="/AWACloud_______/AWACloud_______Group/AWA/CloudToolsDiagnosticAgentVersion" />
          </setting>
        </map>
        <map name="MapAWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" kind="Identity">
          <setting>
            <aCSMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" />
          </setting>
        </map>
        <map name="MapAWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" kind="Identity">
          <setting>
            <aCSMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" />
          </setting>
        </map>
        <map name="MapAWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" kind="Identity">
          <setting>
            <aCSMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" />
          </setting>
        </map>
        <map name="MapAWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" kind="Identity">
          <setting>
            <aCSMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" />
          </setting>
        </map>
        <map name="MapAWA:Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" kind="Identity">
          <setting>
            <aCSMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" />
          </setting>
        </map>
        <map name="MapAWA:Profiling.ProfilingConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Profiling.ProfilingConnectionString" />
          </setting>
        </map>
        <map name="MapAWAInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/AWACloud_______/AWACloud_______Group/AWAInstances" />
          </setting>
        </map>
        <map name="MapCertificate|AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" kind="Identity">
          <certificate>
            <certificateMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
          </certificate>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="AWA" generation="1" functional="0" release="0" software="C:\Users\Rushabh\Documents\Visual Studio 2012\Projects\AWACloud[[[[[[[\AWACloud[[[[[[[\csx\Debug\roles\AWA" entryPoint="base\x86\WaHostBootstrapper.exe" parameters="base\x86\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
              <inPort name="Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" protocol="tcp" />
              <inPort name="Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" protocol="tcp" portRanges="3389" />
              <outPort name="AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/AWACloud_______/AWACloud_______Group/SW:AWA:Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="CloudToolsDiagnosticAgentVersion" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" defaultValue="" />
              <aCS name="Profiling.ProfilingConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;AWA&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;AWA&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EmailIds" defaultAmount="[50,50,50]" defaultSticky="false" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
            <storedcertificates>
              <storedCertificate name="Stored0Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" certificateStore="My" certificateLocation="System">
                <certificate>
                  <certificateMoniker name="/AWACloud_______/AWACloud_______Group/AWA/Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
                </certificate>
              </storedCertificate>
            </storedcertificates>
            <certificates>
              <certificate name="Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
            </certificates>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/AWACloud_______/AWACloud_______Group/AWAInstances" />
            <sCSPolicyFaultDomainMoniker name="/AWACloud_______/AWACloud_______Group/AWAFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyFaultDomain name="AWAFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="AWAInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="08ec13a3-295a-4439-a4b3-2256c4a7dfdc" ref="Microsoft.RedDog.Contract\ServiceContract\AWACloud_______Contract@ServiceDefinition.build">
      <interfacereferences>
        <interfaceReference Id="cb6fd43e-3567-40eb-bf59-dc143a051510" ref="Microsoft.RedDog.Contract\Interface\AWA:Endpoint1@ServiceDefinition.build">
          <inPort>
            <inPortMoniker name="/AWACloud_______/AWACloud_______Group/AWA:Endpoint1" />
          </inPort>
        </interfaceReference>
        <interfaceReference Id="d8bd1f67-ff97-4e4f-92ca-d8568999e84d" ref="Microsoft.RedDog.Contract\Interface\AWA:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput@ServiceDefinition.build">
          <inPort>
            <inPortMoniker name="/AWACloud_______/AWACloud_______Group/AWA:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>