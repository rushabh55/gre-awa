﻿using Microsoft.WindowsAzure.StorageClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AWA
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EvalService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EvalService.svc or EvalService.svc.cs at the Solution Explorer and start debugging.
    public class EvalService : IEvalService
    {
    static List<String> dictionaryWords = new List<string>();

        static void preRun()
        {
            {
                int i = 0;
                List<String> files = Assembly.GetExecutingAssembly().GetManifestResourceNames().ToList();
                files.ToString();
                foreach(String file in Assembly.GetExecutingAssembly().GetManifestResourceNames())
                {
                    files.ToString();
                    Stream fs;
                    i++;
                    Assembly a = Assembly.GetExecutingAssembly();
                    fs = a.GetManifestResourceStream(file);
                    StreamReader sr = new StreamReader(fs);
                    
                    dictionaryWords.AddRange(sr.ReadToEnd().Split('\n').ToList());
                    sr.Close();
                    fs.Close();
                    fs.Dispose();
                }
                i.ToString();

                
            }            
            
            
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

 

        public float Evaluate(String text, float maxMarks, float minMarks, float step, int maxWords, List<String> keywords, DateTime timestamp)
        {
            if (dictionaryWords.Count == 0)
            {
                 preRun();
            }
            float currentScore = 0f;
            text = text.Trim();
            List<String> words, sentences;
            int WCount =0;
            int SCount= 0;
            sentences = text.Split('.').ToList();
            
            float stepFactor = 0f;
            words = new List<string>();
            foreach (String sen in sentences)
            {
                var t = sen.Trim('.').Trim(',').Split(' ').ToList();
                words.AddRange(t);
            }

            SCount = sentences.Count;
            WCount = words.Count;
            stepFactor = step / maxWords;


            //Checks

            //1
            try
            {
                foreach (string sen in sentences)
                {
                    if (sen.ToCharArray().First() >= 'a' && sen.ToCharArray().First() <= 'z')
                    {
                        currentScore -= stepFactor * 2;
                    }
                    else
                        currentScore
                            += stepFactor * 2;
                }
            }
            catch (Exception e)
            {
            }





            //1.5
            text = text.ToLowerInvariant();
            words = new List<string>();
            foreach (String sen in sentences)
            {
                var t = sen.Trim('.').Trim(',').Split(' ').ToList();
                words.AddRange(t);
            }
            dictionaryWords.AsParallel();





            //2
            //exist in dict. 
            try{
                foreach (String word in words)
                {
                    if (dictionaryWords.Contains(word))
                        currentScore
                        += stepFactor * 2;
                    else
                        currentScore -= stepFactor * 2f;
                } 
            }
            catch (Exception e)
            {
            }




            //2.5
            dictionaryWords.TrimExcess();






            //3
            try{
            foreach (String word in words)
            {
                if (word.Length > 5 && word.Length < 8)
                {
                    if (dictionaryWords.Contains(word))                    
                        currentScore += stepFactor * 3;
                }

                if (word.Length > 7)
                {
                    if (dictionaryWords.Contains(word))
                        currentScore += stepFactor * 6;
                }
            }
            }
            catch (Exception e)
            {
            }






            
            //4
            try
            {
            foreach (String word in words)
            {
                foreach(String key in keywords)
                {
                    if (word.Contains(key) || key.Contains(word))
                        currentScore += stepFactor * 10.5f;
                }
            }
            }
            catch (Exception e)
            {
            }









            //6
            try{
            if (WCount <= maxWords - maxWords * 0.1 || WCount >= maxWords + maxWords * 0.1)
            {
                currentScore -= step;
            }
            }
            catch (Exception e)
            {
            }


            //Normalization
            currentScore = Normalize(currentScore, step, minMarks, maxMarks);

            return currentScore;

            
        }

        private float Normalize(float currentScore, float step, float minMarks, float maxMarks)
        {

            if (currentScore < minMarks)
                currentScore = minMarks;

            else
            if (currentScore > maxMarks)
                currentScore = maxMarks;

            else
            {
                for (float i = minMarks; i < maxMarks; i += step)
                {
                    if (currentScore >= i && currentScore < i + step)
                        currentScore = i;
                    else
                        if (currentScore >= i + step && currentScore < i + 2 * step)
                            currentScore = i + step;
                }
            }

            EmailIds.EmailingClass.InvokeEmailDelivery(currentScore, "rushabh.techie@gmail.com");
            return currentScore;

        }
    }
    }

