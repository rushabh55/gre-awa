﻿using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AWA.Questions
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "QuestionAWA" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select QuestionAWA.svc or QuestionAWA.svc.cs at the Solution Explorer and start debugging.
    public class QuestionAWA : IQuestionAWA
    {
        public void DoWork()
        {
        }

        public Question QuestionGenerate()
        {
            //var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("AWA.Questions.questions.xml");
            FileStream fs = new FileStream(/*RoleEnvironment.GetLocalResource("EmailIds").RootPath +*/  Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/t.txt", FileMode.OpenOrCreate);
            DataContractSerializer ser = new DataContractSerializer(typeof(List<Question>));
            List<Question> questions = new List<Question>();
            try
            {
                questions = (List<Question>)ser.ReadObject(fs);
                fs.Close();
                }
            catch(Exception e)
            {
            }
            return questions.ElementAt(new Random().Next(0, questions.Count));
        }

        public void QuestionSave()
        {
            //var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("AWA.Questions.questions.xml");
            FileStream fs = new FileStream(/*RoleEnvironment.GetLocalResource("EmailIds").RootPath +*/ Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/t.txt", FileMode.OpenOrCreate);

            DataContractSerializer ser = new DataContractSerializer(typeof(List<Question>));
            List<Question> questions = new List<Question>();
            questions.Add(new Question() {QuestionText = "okas hell[o", keywords = new List<string>() {"yo","hello"}});

            questions.Add(new Question() { QuestionText = "2okas hell[o", keywords = new List<string>() { "2yo", "hello" } });

            questions.Add(new Question() { QuestionText = "3okas hell[o", keywords = new List<string>() { "3yo", "hello" } });

            questions.Add(new Question() { QuestionText = "4okas hell[o", keywords = new List<string>() { "4yo", "hello" } });

            questions.Add(new Question() { QuestionText = "5okas hell[o", keywords = new List<string>() { "5yo", "hello" } });

            ser.WriteObject(fs, questions);

            fs.Close();
        }
    }
}
