﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="TestCloud" generation="1" functional="0" release="0" Id="17677d38-ba09-42e9-ace5-949b91ba76c9" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="TestCloudGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="DataRole:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/TestCloud/TestCloudGroup/LB:DataRole:Endpoint1" />
          </inToChannel>
        </inPort>
        <inPort name="TestServiceRole:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/TestCloud/TestCloudGroup/LB:TestServiceRole:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="DataRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/TestCloud/TestCloudGroup/MapDataRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="DataRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/TestCloud/TestCloudGroup/MapDataRoleInstances" />
          </maps>
        </aCS>
        <aCS name="MarkEvaluationRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/TestCloud/TestCloudGroup/MapMarkEvaluationRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="MarkEvaluationRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/TestCloud/TestCloudGroup/MapMarkEvaluationRoleInstances" />
          </maps>
        </aCS>
        <aCS name="TestServiceRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/TestCloud/TestCloudGroup/MapTestServiceRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="TestServiceRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/TestCloud/TestCloudGroup/MapTestServiceRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:DataRole:Endpoint1">
          <toPorts>
            <inPortMoniker name="/TestCloud/TestCloudGroup/DataRole/Endpoint1" />
          </toPorts>
        </lBChannel>
        <lBChannel name="LB:TestServiceRole:Endpoint1">
          <toPorts>
            <inPortMoniker name="/TestCloud/TestCloudGroup/TestServiceRole/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapDataRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/TestCloud/TestCloudGroup/DataRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapDataRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/TestCloud/TestCloudGroup/DataRoleInstances" />
          </setting>
        </map>
        <map name="MapMarkEvaluationRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/TestCloud/TestCloudGroup/MarkEvaluationRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapMarkEvaluationRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/TestCloud/TestCloudGroup/MarkEvaluationRoleInstances" />
          </setting>
        </map>
        <map name="MapTestServiceRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/TestCloud/TestCloudGroup/TestServiceRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapTestServiceRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/TestCloud/TestCloudGroup/TestServiceRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="DataRole" generation="1" functional="0" release="0" software="C:\Users\Rush\documents\visual studio 2012\Projects\TestCloud\TestCloud\csx\Debug\roles\DataRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="8080" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;DataRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;DataRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;MarkEvaluationRole&quot; /&gt;&lt;r name=&quot;TestServiceRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/TestCloud/TestCloudGroup/DataRoleInstances" />
            <sCSPolicyFaultDomainMoniker name="/TestCloud/TestCloudGroup/DataRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="MarkEvaluationRole" generation="1" functional="0" release="0" software="C:\Users\Rush\documents\visual studio 2012\Projects\TestCloud\TestCloud\csx\Debug\roles\MarkEvaluationRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="1792" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;MarkEvaluationRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;DataRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;MarkEvaluationRole&quot; /&gt;&lt;r name=&quot;TestServiceRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/TestCloud/TestCloudGroup/MarkEvaluationRoleInstances" />
            <sCSPolicyFaultDomainMoniker name="/TestCloud/TestCloudGroup/MarkEvaluationRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="TestServiceRole" generation="1" functional="0" release="0" software="C:\Users\Rush\documents\visual studio 2012\Projects\TestCloud\TestCloud\csx\Debug\roles\TestServiceRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;TestServiceRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;DataRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;MarkEvaluationRole&quot; /&gt;&lt;r name=&quot;TestServiceRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="TestServiceRole.svclog" defaultAmount="[1000,1000,1000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/TestCloud/TestCloudGroup/TestServiceRoleInstances" />
            <sCSPolicyFaultDomainMoniker name="/TestCloud/TestCloudGroup/TestServiceRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyFaultDomain name="DataRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="MarkEvaluationRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="TestServiceRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="DataRoleInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="MarkEvaluationRoleInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="TestServiceRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="a5d2b993-1e56-4910-82b9-1100a8246dc9" ref="Microsoft.RedDog.Contract\ServiceContract\TestCloudContract@ServiceDefinition.build">
      <interfacereferences>
        <interfaceReference Id="e7f0281e-2847-4d35-aafe-a61ff2c293e0" ref="Microsoft.RedDog.Contract\Interface\DataRole:Endpoint1@ServiceDefinition.build">
          <inPort>
            <inPortMoniker name="/TestCloud/TestCloudGroup/DataRole:Endpoint1" />
          </inPort>
        </interfaceReference>
        <interfaceReference Id="00cc4ae1-6b4d-4558-a87f-14585050ca6e" ref="Microsoft.RedDog.Contract\Interface\TestServiceRole:Endpoint1@ServiceDefinition.build">
          <inPort>
            <inPortMoniker name="/TestCloud/TestCloudGroup/TestServiceRole:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>