﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataRole.Questions
{
    public class Questions
    {
        public String Question;
        public QuestionType type;
        public String CorrectAnswer;
        public int correctionCoefficient = 0;
        public List<String> options = new List<string>();
        public String remarks;
        public int totalMarks = 0;
        public int minimumMarks = 0;
        public int maximumMarks = 0;
        public int markThreshold = 1;
        public List<String> keywords = new List<string>();

        public static bool RationalizeQuestions(Questions question)
        {
            if (question.minimumMarks > question.maximumMarks || question.maximumMarks > question.totalMarks || question.totalMarks < question.maximumMarks || question.markThreshold > question.maximumMarks / 2 || question.totalMarks <= 0 || question.minimumMarks <= 0)
                return false;

            if (String.Equals(question.Question, String.Empty))
                return false;

            if (question.correctionCoefficient < 0)
                return false;

            switch (question.type)
            {
                case QuestionType.MultipleChoice: 
                    if (question.options.Count <= 2)
                        return false;

                    if (!question.options.Contains(question.CorrectAnswer))
                        return false;

                    if (question.keywords.Count > 0)
                        return false;
                    break;
                case QuestionType.Objective :
                    if (String.IsNullOrEmpty(question.CorrectAnswer))
                        return false;
                    
                    if (String.Equals(question.CorrectAnswer, String.Empty))
                        return false;
                    
                    break;
                case QuestionType.Subjective:
                    if (String.IsNullOrEmpty(question.CorrectAnswer))
                        return false;
                    break;
            }


            return true;
        }
    }
}


public enum QuestionType
{
    MultipleChoice,
    Objective,
    Subjective
}