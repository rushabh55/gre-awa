//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataRole.App_Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Test
    {
        public Test()
        {
            this.Users = new HashSet<Users>();
        }
    
        public int Id { get; set; }
        public string Questions { get; set; }
        public string Creator { get; set; }
        public string ValidityTime { get; set; }
        public string PostTime { get; set; }
    
        public virtual ICollection<Users> Users { get; set; }
    }
}
