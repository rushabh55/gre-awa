﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataRole
{
    public class User
    {
        public string UserName;
        public string Password;
        public String Email;
        public int UserId;
    }

    public enum ResultType
    {
        LoggedIn,
        Registered,
        AlreadyExists,
        Unavailable,
        NoSuchUser
    }

    public enum UserPrivilageLevel
    {
        Administrator,
        Editor,
        Student
    }
}