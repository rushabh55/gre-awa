﻿using DataRole;
using DataRole.Questions;
using DataRole.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TestServiceRole.Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "QuestionsM" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select QuestionsM.svc or QuestionsM.svc.cs at the Solution Explorer and start debugging.
    public class QuestionsM : IQuestionsM
    {
        public int CreateTest(int UserId, Test test, User user)
        {
            UserM u = new UserM(user);
            if (!u.ValidateUser())
                return -1;

            foreach (Questions q in test.QuestionBase)
            {
                if (!Questions.RationalizeQuestions(q))
                {
                    return -1;
                }
            }

            if (test.dueTime == null)
            {
                return -1;
            }

            if (test.timeCreated.CompareTo(DateTime.Now) < 0)
                return -1;

            FileStream fs = new FileStream("/App_Data/Data.xml", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            DataContractSerializer DCS = new DataContractSerializer(typeof(List<Test>));

            try
            {
                var t = (List<Test>)(DCS.ReadObject(fs));
                t.Add(test);
                DCS.WriteObject(fs, t);
            }
            catch (Exception _e)
            {
            }

            return 1;
        }



        public bool AddQuestion(int id, int UserId, int QuestionNo, DataRole.Questions.Questions question)
        {
            return true;
        }


        public int getNewTestId(int UserId)
        {
            return 1;
        }
    }
}
