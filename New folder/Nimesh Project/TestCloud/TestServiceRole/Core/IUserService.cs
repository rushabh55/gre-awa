﻿using DataRole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TestServiceRole.Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserService" in both code and config file together.
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        ResultType Login(String UserName, String Password);

        [OperationContract]
        ResultType Register(String UserName, String Password, String Email);
    }
}
