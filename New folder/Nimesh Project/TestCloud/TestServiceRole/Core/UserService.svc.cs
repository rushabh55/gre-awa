﻿using DataRole;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TestServiceRole.Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select UserService.svc or UserService.svc.cs at the Solution Explorer and start debugging.
    public class UserService : IUserService
    {
        public ResultType Login(String UserName, String Password)
        {
            String Path = Environment.CurrentDirectory + @"/App_Data/Users.xml";


            FileStream fs = new FileStream(Path, FileMode.Open, FileAccess.ReadWrite);
            Stream.Synchronized(fs);
            List<DataRole.User> currentUsers = new List<DataRole.User>();
            DataContractSerializer dcs = new DataContractSerializer(typeof(List<DataRole.User>));
            try
            {
                currentUsers = (List<DataRole.User>)dcs.ReadObject(fs);
            }
            catch (Exception)
            {

            }
            finally
            {
                fs.Close();
                fs.Dispose();
            }
            foreach (var value in currentUsers)
            {
                if (String.Equals(value.UserName, UserName) && String.Equals(value.Password, Password))
                {
                    return ResultType.LoggedIn;
                }
            }

            return ResultType.NoSuchUser;
        }
       
               
        

        public ResultType Register(String UserName, String Password, String Email)
        {
            String Path = Environment.CurrentDirectory + @"/App_Data/Users.xml";

            FileStream fs = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            Stream.Synchronized(fs);
            
            List<DataRole.User> currentUsers = new List<DataRole.User>();
            DataContractSerializer dcs = new DataContractSerializer(typeof(List<DataRole.User>));
            try
            {
                currentUsers = (List<DataRole.User>)dcs.ReadObject(fs);
            }
            catch (Exception)
            {

            }
            finally
            {
                fs.Close();
                fs.Dispose();
            }
            if (Login(UserName, Password) == ResultType.LoggedIn)
            {
                return ResultType.AlreadyExists;
            }

            fs = new FileStream(Path, FileMode.Open, FileAccess.ReadWrite);
           
            currentUsers.Add(new DataRole.User() { UserName = UserName, Password = Password, Email = Email });
            fs.Seek(0, SeekOrigin.Begin);
            dcs.WriteObject(fs, currentUsers);
            fs.Close();
            fs.Dispose();
            return ResultType.Registered;
        }
    }
}
