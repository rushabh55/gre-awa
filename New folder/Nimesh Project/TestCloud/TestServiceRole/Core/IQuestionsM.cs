﻿using DataRole;
using DataRole.Questions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TestServiceRole.Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IQuestionsM" in both code and config file together.
    [ServiceContract]
    public interface IQuestionsM
    {
        [OperationContract]
        bool AddQuestion(int id, int UserId, int QuestionNo, DataRole.Questions.Questions question);

        [OperationContract]
        int CreateTest(int UserId, Test test, User user);

        [OperationContract]
        int getNewTestId(int UserId);

    }
}
