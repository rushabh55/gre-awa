﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TestClient.Forms;
using TestClient.Forms.TestBuilder;
using TestClient.Forms.User_Operation;

namespace TestClient
{
    public partial class MainWindow : Form
    {
        UserService.UserServiceClient client = new UserService.UserServiceClient();
       
        public MainWindow()
        {
            InitializeComponent();
            
           
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            client.LoginCompleted += client_LoginCompleted;
            
            client.LoginAsync(userNameBox.Text, passwordBox.Text);
            
        }

        void client_LoginCompleted(object sender, UserService.LoginCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == UserService.ResultType.LoggedIn)
                {
                    ProfessorOptions t = new ProfessorOptions();
                    t.Activate();
                    t.Visible = true;
                    t.Enabled = true;
                    this.Visible = false;
                }
            }
        }

        private void registerModeToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void loginModeToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void loginModeToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {

        }

        private void registerModeToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            Register r = new Register();
            r.Enabled = true;
            r.Visible = true;
        }
    }
}
