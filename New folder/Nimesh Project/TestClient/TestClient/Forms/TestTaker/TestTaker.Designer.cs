﻿namespace TestClient.Forms.TestTaker
{
    partial class TestTaker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timePane = new System.Windows.Forms.Label();
            this.user = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.QuestionsList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.help = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(12, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1004, 121);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(159, 91);
            this.button1.TabIndex = 0;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(180, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(159, 91);
            this.button2.TabIndex = 1;
            this.button2.Text = "End Test";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(345, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(159, 91);
            this.button3.TabIndex = 2;
            this.button3.Text = "Pause";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(839, 13);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(159, 91);
            this.button4.TabIndex = 5;
            this.button4.Text = "Next";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(674, 13);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(159, 91);
            this.button5.TabIndex = 4;
            this.button5.Text = "Show Time";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(509, 13);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(159, 91);
            this.button6.TabIndex = 3;
            this.button6.Text = "Mark";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            // 
            // timePane
            // 
            this.timePane.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.timePane.AutoSize = true;
            this.timePane.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.timePane.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.timePane.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.timePane.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.timePane.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timePane.Location = new System.Drawing.Point(924, 9);
            this.timePane.Name = "timePane";
            this.timePane.Size = new System.Drawing.Size(2, 33);
            this.timePane.TabIndex = 1;
            // 
            // user
            // 
            this.user.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.user.AutoSize = true;
            this.user.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.user.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.user.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.user.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.user.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user.Location = new System.Drawing.Point(27, 9);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(74, 33);
            this.user.TabIndex = 2;
            this.user.Text = "User";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.QuestionsList);
            this.groupBox1.Location = new System.Drawing.Point(12, 184);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1004, 526);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Test";
            // 
            // QuestionsList
            // 
            this.QuestionsList.FormattingEnabled = true;
            this.QuestionsList.Location = new System.Drawing.Point(15, 19);
            this.QuestionsList.Name = "QuestionsList";
            this.QuestionsList.Size = new System.Drawing.Size(233, 485);
            this.QuestionsList.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AllowDrop = true;
            this.label1.AutoEllipsis = true;
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.ContextMenuStrip = this.help;
            this.label1.Enabled = false;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(265, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Question";
            this.label1.UseCompatibleTextRendering = true;
            // 
            // help
            // 
            this.help.Name = "help";
            this.help.Size = new System.Drawing.Size(61, 4);
            this.help.Text = "Help";
            // 
            // TestTaker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 729);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.user);
            this.Controls.Add(this.timePane);
            this.Controls.Add(this.panel1);
            this.Name = "TestTaker";
            this.Text = "TestTaker";
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label timePane;
        private System.Windows.Forms.Label user;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip help;
        private System.Windows.Forms.ListBox QuestionsList;
    }
}