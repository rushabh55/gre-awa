﻿namespace TestClient.Forms
{
    partial class ProfessorOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newTest = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // newTest
            // 
            this.newTest.Location = new System.Drawing.Point(420, 103);
            this.newTest.Name = "newTest";
            this.newTest.Size = new System.Drawing.Size(223, 149);
            this.newTest.TabIndex = 0;
            this.newTest.Text = "Create a New Test";
            this.newTest.UseVisualStyleBackColor = true;
            this.newTest.Click += new System.EventHandler(this.newTest_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(420, 258);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(223, 149);
            this.button1.TabIndex = 1;
            this.button1.Text = "Add/Edit/Remove a new Question to a previous Test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(420, 413);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(223, 149);
            this.button2.TabIndex = 2;
            this.button2.Text = "Continue from a Locally Saved Test";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ProfessorOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 729);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.newTest);
            this.Name = "ProfessorOptions";
            this.Text = "ProfessorOptions";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button newTest;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}