﻿namespace TestClient.Forms.User_Operation
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserName = new System.Windows.Forms.TextBox();
            this.Pass1 = new System.Windows.Forms.TextBox();
            this.Pass2 = new System.Windows.Forms.TextBox();
            this.Email = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UserName
            // 
            this.UserName.Location = new System.Drawing.Point(555, 351);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(674, 20);
            this.UserName.TabIndex = 0;
            this.UserName.Text = "UserName";
            // 
            // Pass1
            // 
            this.Pass1.Location = new System.Drawing.Point(555, 400);
            this.Pass1.Name = "Pass1";
            this.Pass1.Size = new System.Drawing.Size(674, 20);
            this.Pass1.TabIndex = 1;
            this.Pass1.Text = "Password";
            // 
            // Pass2
            // 
            this.Pass2.Location = new System.Drawing.Point(555, 446);
            this.Pass2.Name = "Pass2";
            this.Pass2.Size = new System.Drawing.Size(674, 20);
            this.Pass2.TabIndex = 2;
            this.Pass2.Text = "Renter ";
            // 
            // Email
            // 
            this.Email.Location = new System.Drawing.Point(555, 496);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(674, 20);
            this.Email.TabIndex = 3;
            this.Email.Text = "Email";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(762, 575);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(240, 77);
            this.button1.TabIndex = 4;
            this.button1.Text = "Register";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Email);
            this.Controls.Add(this.Pass2);
            this.Controls.Add(this.Pass1);
            this.Controls.Add(this.UserName);
            this.Name = "Register";
            this.Text = "Register";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox UserName;
        private System.Windows.Forms.TextBox Pass1;
        private System.Windows.Forms.TextBox Pass2;
        private System.Windows.Forms.TextBox Email;
        private System.Windows.Forms.Button button1;
    }
}