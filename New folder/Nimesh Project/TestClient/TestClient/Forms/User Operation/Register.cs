﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestClient.Forms.User_Operation
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Pass1.Text == Pass2.Text)
            {
                UserService.UserServiceClient a = new UserService.UserServiceClient();
                var t = a.Register(UserName.Text, Pass1.Text, Email.Text);
                if (t == UserService.ResultType.Registered)
                    MessageBox.Show("Yo");
            }
        }
    }
}
