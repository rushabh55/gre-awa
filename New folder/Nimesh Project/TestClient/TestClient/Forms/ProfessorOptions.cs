﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TestClient.Forms.TestBuilder;

namespace TestClient.Forms
{
    public partial class ProfessorOptions : Form
    {
        public ProfessorOptions()
        {
            InitializeComponent();
        }

        private void newTest_Click(object sender, EventArgs e)
        {
            TBuilder t = new TBuilder();
            t.Activate();
            t.Visible = true;
            t.Enabled = true;
            this.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            var t = openFileDialog1.OpenFile();
            TBuilder a = new TBuilder();
            a.openFile = new MemoryStream();
            Stream.Synchronized(a.openFile);
            t.CopyTo(a.openFile);
            a.Activate();
            a.LoadFile();
            this.Visible = false;
        }
    }
}
