﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Forms;
using TestClient.QuestionService;

namespace TestClient.Forms.TestBuilder
{
    public partial class TBuilder : Form
    {
        List<QuestionService.Questions> Questions = new List<QuestionService.Questions>();
        QuestionService.QuestionsMClient ServiceClient = new QuestionService.QuestionsMClient();
        public Stream openFile = Stream.Null;
        public TBuilder()
        {
            InitializeComponent();
            checkedListBox1.CheckOnClick = false;
            checkedListBox1.ThreeDCheckBoxes = true;
        }

        public void LoadFile()
        {
            try
            {
                if (openFile != null)
                    if (openFile.Length > 2)
                    {
                        DataContractSerializer DCS = new DataContractSerializer(typeof(List<QuestionService.Questions>));
                        try
                        {
                            var test = (List<QuestionService.Questions>)DCS.ReadObject(openFile);
                            foreach (var quest in test)
                            {
                                question.Text = quest.Question;
                                correctAns.Text = quest.CorrectAnswer;
                                keywords.Text = quest.keywords.ToString();
                                totalMarks.Text = quest.maximumMarks.ToString();
                                minMarks.Text = quest.minimumMarks.ToString();
                                optionBox.Items.Clear();
                                optionBox.Items.AddRange(quest.options);
                            }

                            Questions = test;
                        }
                        catch (Exception _e)
                        {
                            MessageBox.Show("The file has not been saved correctly, we may not be able to retrieve the data :(");
                        }

                    }
            }
            catch (Exception _e)
            {
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                var quest = Questions.ElementAt(checkedListBox1.SelectedIndex);
                question.Text = quest.Question;
                correctAns.Text = quest.CorrectAnswer;
                keywords.Text = quest.keywords.ToString();
                totalMarks.Text = quest.maximumMarks.ToString();
                minMarks.Text = quest.minimumMarks.ToString();
                optionBox.Items.Clear();
                optionBox.Items.AddRange(quest.options);
            }
            catch (Exception _e)
            {
                question.ResetText();
                correctAns.ResetText();
                keywords.ResetText();
                totalMarks.ResetText();
                minMarks.ResetText();
                optionBox.Items.Clear();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkedListBox1.Items.Count > int.Parse(totQuest.Text))
                {
                    var res = MessageBox.Show("Do you want to delete the extra items?", "Are you sure?", MessageBoxButtons.YesNo);
                    if (res == System.Windows.Forms.DialogResult.No)
                        return;
                    for (int i = checkedListBox1.Items.Count; i > int.Parse(totQuest.Text); i--)
                    {
                        checkedListBox1.Items.RemoveAt(i);
                    }
                }

                else
                    if (checkedListBox1.Items.Count < int.Parse(totQuest.Text))
                    {
                        for (int i = checkedListBox1.Items.Count; i < int.Parse(totQuest.Text); i++)
                            checkedListBox1.Items.Add("Question " + i);
                    }
                    else
                    {
                        return;
                    }

            }
            catch (Exception _e)
            {
                MessageBox.Show(Error.error1);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            optionBox.Items.Add(newOption.Text);
        }

        private void optionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (optionBox.SelectedItem == null)
                remove.Enabled = false;
            else
                remove.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var index = checkedListBox1.SelectedIndex;
                var ques = new QuestionService.Questions();
                ques.CorrectAnswer = question.Text.Trim();
                ques.correctionCoefficient = 0;
                ques.keywords = keywords.Text.Trim().Split(',');
                ques.markThreshold = int.Parse(stepInc.Text);
                ques.maximumMarks = int.Parse(totalMarks.Text);
                ques.minimumMarks = int.Parse(minMarks.Text);
                List<String> optionsList = new List<string>();
                foreach (var item in optionBox.Items)
                {
                    optionsList.Add(item.ToString());
                }
                ques.options = optionsList.ToArray();
                ques.Question = question.Text.Trim();
                ques.remarks = remarksBox.Text.Trim();
                ques.type = QuestionService.QuestionType.MultipleChoice;
                Questions.Add(ques);
                checkedListBox1.Items.RemoveAt(index);
                checkedListBox1.Items.Insert(index, ques.Question + "(Added)");
                var t = checkedListBox1.CheckedItems.Cast<List<bool>>();
                t.ToString();
            }
            catch (Exception _e)
            {
                MessageBox.Show(Error.formValidation + _e.Message, "Incorrect Format");
            }
        }

        private void remove_Click(object sender, EventArgs e)
        {
            optionBox.Items.RemoveAt(optionBox.SelectedIndex);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                var index = checkedListBox1.SelectedIndex;
                var ques = new QuestionService.Questions();
                ques.CorrectAnswer = question.Text.Trim();
                ques.correctionCoefficient = 0;
                ques.keywords = keywords.Text.Trim().Split(',');
                ques.markThreshold = int.Parse(stepInc.Text);
                ques.maximumMarks = int.Parse(totalMarks.Text);
                ques.minimumMarks = int.Parse(minMarks.Text);
                List<String> optionsList = new List<string>();
                foreach (var item in optionBox.Items)
                {
                    optionsList.Add(item.ToString());
                }
                ques.options = optionsList.ToArray();
                ques.Question = question.Text.Trim();
                ques.remarks = remarksBox.Text.Trim();
                ques.type = QuestionService.QuestionType.Objective;
                Questions.Add(ques);
                checkedListBox1.Items.RemoveAt(index);
                checkedListBox1.Items.Insert(index, ques.Question + "(Added)");
                var t = checkedListBox1.CheckedItems.Cast<List<bool>>();
                t.ToString();
            }
            catch (Exception _e)
            {
                MessageBox.Show(Error.formValidation + _e.Message, "Incorrect Format");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                var index = checkedListBox1.SelectedIndex;
                var ques = new QuestionService.Questions();
                ques.CorrectAnswer = question.Text.Trim();
                ques.correctionCoefficient = 0;
                ques.keywords = keywords.Text.Trim().Split(',');
                ques.markThreshold = int.Parse(stepInc.Text);
                ques.maximumMarks = int.Parse(totalMarks.Text);
                ques.minimumMarks = int.Parse(minMarks.Text);
                List<String> optionsList = new List<string>();
                foreach (var item in optionBox.Items)
                {
                    optionsList.Add(item.ToString());
                }
                ques.options = optionsList.ToArray();
                ques.Question = question.Text.Trim();
                ques.remarks = remarksBox.Text.Trim();
                ques.type = QuestionService.QuestionType.Subjective;
                Questions.Add(ques);
                checkedListBox1.Items.RemoveAt(index);
                checkedListBox1.Items.Insert(index, ques.Question + "(Added)");
                var t = checkedListBox1.CheckedItems.Cast<List<bool>>();
                t.ToString();
            }
            catch (Exception _e)
            {
                MessageBox.Show(Error.formValidation + _e.Message, "Incorrect Format");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var t = Convert.ToDateTime(timeLimit.Text.Trim());
                
            }
            catch (Exception _e)
            {
                MessageBox.Show(Error.formValidation + _e.Message, "Incorrect Format");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int i;
            Test test = new Test() { QuestionBase = Questions.ToArray(), Author = new User(), timeCreated = DateTime.Now, UId = new Guid() }; 
            if ((i = ServiceClient.CreateTest(1, test, new User())) != -1)
            {
                MessageBox.Show("Successfully Registered Test. Please note the following Test Id for future reference" + i, "Success");
                this.Visible = false;
                ProfessorOptions op = new ProfessorOptions();
                op.Activate();
                op.Visible = true;
            }
            else
            {
                MessageBox.Show("Server Not reachable", "Error");
            }
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                saveFile.ShowDialog();
                var t = saveFile.OpenFile();
                DataContractSerializer DCS = new DataContractSerializer(typeof(List<QuestionService.Questions>));
                DCS.WriteObject(t, Questions);
                t.Close();

                this.Visible = false;
                ProfessorOptions op = new ProfessorOptions();
                op.Activate();
                op.Visible = true;
            }
            catch (Exception _e)
            {
                MessageBox.Show(Error.error1, "Error!");
            }
        }
    }
}
