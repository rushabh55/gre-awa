﻿namespace TestClient.Forms.TestBuilder
{
    partial class TBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.totQuest = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timeLimit = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.testName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.author = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.mcq = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.correctAns = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.keywords = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.stepInc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.minMarks = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.totalMarks = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.remarksBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.remove = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.newOption = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.optionBox = new System.Windows.Forms.ListBox();
            this.question = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.objective = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.subjective = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.saveFile = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1.SuspendLayout();
            this.mcq.SuspendLayout();
            this.objective.SuspendLayout();
            this.subjective.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.AllowDrop = true;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(6, 91);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(206, 484);
            this.checkedListBox1.TabIndex = 1;
            this.checkedListBox1.ThreeDCheckBoxes = true;
            this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(352, 42);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 20);
            this.button1.TabIndex = 2;
            this.button1.Text = "Set";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // totQuest
            // 
            this.totQuest.Location = new System.Drawing.Point(223, 42);
            this.totQuest.Name = "totQuest";
            this.totQuest.Size = new System.Drawing.Size(123, 20);
            this.totQuest.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(220, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Total Questions";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(513, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Total Time";
            // 
            // timeLimit
            // 
            this.timeLimit.Location = new System.Drawing.Point(516, 42);
            this.timeLimit.Name = "timeLimit";
            this.timeLimit.Size = new System.Drawing.Size(116, 20);
            this.timeLimit.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(638, 42);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 20);
            this.button2.TabIndex = 7;
            this.button2.Text = "Set";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // testName
            // 
            this.testName.AutoSize = true;
            this.testName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testName.Location = new System.Drawing.Point(3, 42);
            this.testName.Name = "testName";
            this.testName.Size = new System.Drawing.Size(0, 22);
            this.testName.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Author";
            // 
            // author
            // 
            this.author.AutoSize = true;
            this.author.Location = new System.Drawing.Point(3, 75);
            this.author.Name = "author";
            this.author.Size = new System.Drawing.Size(0, 13);
            this.author.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.mcq);
            this.tabControl1.Controls.Add(this.objective);
            this.tabControl1.Controls.Add(this.subjective);
            this.tabControl1.Location = new System.Drawing.Point(223, 91);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(793, 487);
            this.tabControl1.TabIndex = 11;
            // 
            // mcq
            // 
            this.mcq.Controls.Add(this.label12);
            this.mcq.Controls.Add(this.correctAns);
            this.mcq.Controls.Add(this.button4);
            this.mcq.Controls.Add(this.keywords);
            this.mcq.Controls.Add(this.label11);
            this.mcq.Controls.Add(this.stepInc);
            this.mcq.Controls.Add(this.label10);
            this.mcq.Controls.Add(this.minMarks);
            this.mcq.Controls.Add(this.label9);
            this.mcq.Controls.Add(this.totalMarks);
            this.mcq.Controls.Add(this.label8);
            this.mcq.Controls.Add(this.remarksBox);
            this.mcq.Controls.Add(this.label7);
            this.mcq.Controls.Add(this.remove);
            this.mcq.Controls.Add(this.button3);
            this.mcq.Controls.Add(this.newOption);
            this.mcq.Controls.Add(this.label5);
            this.mcq.Controls.Add(this.optionBox);
            this.mcq.Controls.Add(this.question);
            this.mcq.Controls.Add(this.label3);
            this.mcq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcq.Location = new System.Drawing.Point(4, 22);
            this.mcq.Name = "mcq";
            this.mcq.Padding = new System.Windows.Forms.Padding(3);
            this.mcq.Size = new System.Drawing.Size(785, 461);
            this.mcq.TabIndex = 0;
            this.mcq.Text = "Multiple Choice";
            this.mcq.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(408, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(372, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Correct Answer(Must be exactly similar to one of the option from avail Options)";
            // 
            // correctAns
            // 
            this.correctAns.Location = new System.Drawing.Point(411, 31);
            this.correctAns.Name = "correctAns";
            this.correctAns.Size = new System.Drawing.Size(334, 20);
            this.correctAns.TabIndex = 18;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(670, 379);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 17;
            this.button4.Text = "Save";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // keywords
            // 
            this.keywords.AcceptsReturn = true;
            this.keywords.AcceptsTab = true;
            this.keywords.AllowDrop = true;
            this.keywords.Location = new System.Drawing.Point(411, 254);
            this.keywords.MinimumSize = new System.Drawing.Size(4, 150);
            this.keywords.Name = "keywords";
            this.keywords.Size = new System.Drawing.Size(334, 20);
            this.keywords.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(408, 238);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(167, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Keywords(Seperate with Commas)";
            // 
            // stepInc
            // 
            this.stepInc.Location = new System.Drawing.Point(411, 198);
            this.stepInc.Name = "stepInc";
            this.stepInc.Size = new System.Drawing.Size(100, 20);
            this.stepInc.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(408, 182);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Mark Step(Increase Coefficient)";
            // 
            // minMarks
            // 
            this.minMarks.Location = new System.Drawing.Point(411, 139);
            this.minMarks.Name = "minMarks";
            this.minMarks.Size = new System.Drawing.Size(100, 20);
            this.minMarks.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(408, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Minimum Marks";
            // 
            // totalMarks
            // 
            this.totalMarks.Location = new System.Drawing.Point(411, 81);
            this.totalMarks.Name = "totalMarks";
            this.totalMarks.Size = new System.Drawing.Size(100, 20);
            this.totalMarks.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(408, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Total Marks";
            // 
            // remarksBox
            // 
            this.remarksBox.AcceptsReturn = true;
            this.remarksBox.AcceptsTab = true;
            this.remarksBox.AllowDrop = true;
            this.remarksBox.Location = new System.Drawing.Point(9, 254);
            this.remarksBox.MinimumSize = new System.Drawing.Size(4, 150);
            this.remarksBox.Name = "remarksBox";
            this.remarksBox.Size = new System.Drawing.Size(334, 20);
            this.remarksBox.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Remarks(Optional)";
            // 
            // remove
            // 
            this.remove.Enabled = false;
            this.remove.Location = new System.Drawing.Point(308, 191);
            this.remove.Name = "remove";
            this.remove.Size = new System.Drawing.Size(35, 20);
            this.remove.TabIndex = 6;
            this.remove.Text = "-";
            this.remove.UseVisualStyleBackColor = true;
            this.remove.Click += new System.EventHandler(this.remove_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(269, 191);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 20);
            this.button3.TabIndex = 5;
            this.button3.Text = "+";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // newOption
            // 
            this.newOption.Location = new System.Drawing.Point(9, 191);
            this.newOption.Name = "newOption";
            this.newOption.Size = new System.Drawing.Size(251, 20);
            this.newOption.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Available Options";
            // 
            // optionBox
            // 
            this.optionBox.FormattingEnabled = true;
            this.optionBox.Location = new System.Drawing.Point(9, 90);
            this.optionBox.Name = "optionBox";
            this.optionBox.Size = new System.Drawing.Size(334, 95);
            this.optionBox.TabIndex = 2;
            this.optionBox.SelectedIndexChanged += new System.EventHandler(this.optionBox_SelectedIndexChanged);
            // 
            // question
            // 
            this.question.Location = new System.Drawing.Point(9, 31);
            this.question.Name = "question";
            this.question.Size = new System.Drawing.Size(334, 20);
            this.question.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Question";
            // 
            // objective
            // 
            this.objective.Controls.Add(this.button5);
            this.objective.Controls.Add(this.label15);
            this.objective.Controls.Add(this.textBox1);
            this.objective.Controls.Add(this.textBox3);
            this.objective.Controls.Add(this.label16);
            this.objective.Controls.Add(this.textBox4);
            this.objective.Controls.Add(this.label17);
            this.objective.Controls.Add(this.textBox5);
            this.objective.Controls.Add(this.label18);
            this.objective.Controls.Add(this.textBox6);
            this.objective.Controls.Add(this.label19);
            this.objective.Controls.Add(this.textBox7);
            this.objective.Controls.Add(this.label20);
            this.objective.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.objective.Location = new System.Drawing.Point(4, 22);
            this.objective.Name = "objective";
            this.objective.Padding = new System.Windows.Forms.Padding(3);
            this.objective.Size = new System.Drawing.Size(785, 461);
            this.objective.TabIndex = 1;
            this.objective.Text = "Objective";
            this.objective.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(670, 429);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 32;
            this.button5.Text = "Save";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(407, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "Correct Answer";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(410, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(334, 20);
            this.textBox1.TabIndex = 30;
            // 
            // textBox3
            // 
            this.textBox3.AcceptsReturn = true;
            this.textBox3.AcceptsTab = true;
            this.textBox3.AllowDrop = true;
            this.textBox3.Location = new System.Drawing.Point(410, 262);
            this.textBox3.MinimumSize = new System.Drawing.Size(4, 150);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(334, 20);
            this.textBox3.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(407, 246);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(167, 13);
            this.label16.TabIndex = 28;
            this.label16.Text = "Keywords(Seperate with Commas)";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(410, 147);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 27;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(407, 131);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 13);
            this.label17.TabIndex = 26;
            this.label17.Text = "Minimum Marks";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(8, 147);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 25;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 131);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Total Marks";
            // 
            // textBox6
            // 
            this.textBox6.AcceptsReturn = true;
            this.textBox6.AcceptsTab = true;
            this.textBox6.AllowDrop = true;
            this.textBox6.Location = new System.Drawing.Point(8, 262);
            this.textBox6.MinimumSize = new System.Drawing.Size(4, 150);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(334, 20);
            this.textBox6.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 246);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "Remarks(Optional)";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(8, 39);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(334, 20);
            this.textBox7.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(5, 23);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "Question";
            // 
            // subjective
            // 
            this.subjective.Controls.Add(this.button6);
            this.subjective.Controls.Add(this.textBox8);
            this.subjective.Controls.Add(this.label21);
            this.subjective.Controls.Add(this.textBox9);
            this.subjective.Controls.Add(this.label22);
            this.subjective.Controls.Add(this.textBox10);
            this.subjective.Controls.Add(this.label23);
            this.subjective.Controls.Add(this.textBox11);
            this.subjective.Controls.Add(this.label24);
            this.subjective.Controls.Add(this.textBox12);
            this.subjective.Controls.Add(this.label25);
            this.subjective.Controls.Add(this.textBox13);
            this.subjective.Controls.Add(this.label26);
            this.subjective.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjective.Location = new System.Drawing.Point(4, 22);
            this.subjective.Name = "subjective";
            this.subjective.Padding = new System.Windows.Forms.Padding(3);
            this.subjective.Size = new System.Drawing.Size(785, 461);
            this.subjective.TabIndex = 2;
            this.subjective.Text = "Subjective";
            this.subjective.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(690, 418);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(72, 42);
            this.button6.TabIndex = 29;
            this.button6.Text = "Save";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // textBox8
            // 
            this.textBox8.AcceptsReturn = true;
            this.textBox8.AcceptsTab = true;
            this.textBox8.AllowDrop = true;
            this.textBox8.Location = new System.Drawing.Point(428, 262);
            this.textBox8.MinimumSize = new System.Drawing.Size(4, 150);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(334, 20);
            this.textBox8.TabIndex = 28;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(425, 246);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(241, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Keywords(Seperate with Commas, Very Important)";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(428, 142);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 26;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(425, 126);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(156, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "Mark Step(Increase Coefficient)";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(26, 142);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 24;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(23, 126);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(80, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "Minimum Marks";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(428, 39);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 22;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(425, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 13);
            this.label24.TabIndex = 21;
            this.label24.Text = "Total Marks";
            // 
            // textBox12
            // 
            this.textBox12.AcceptsReturn = true;
            this.textBox12.AcceptsTab = true;
            this.textBox12.AllowDrop = true;
            this.textBox12.Location = new System.Drawing.Point(26, 262);
            this.textBox12.MinimumSize = new System.Drawing.Size(4, 150);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(334, 20);
            this.textBox12.TabIndex = 20;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(23, 246);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(133, 13);
            this.label25.TabIndex = 19;
            this.label25.Text = "References/Points/Topics";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(26, 39);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(334, 20);
            this.textBox13.TabIndex = 18;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(23, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 13);
            this.label26.TabIndex = 17;
            this.label26.Text = "Question";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(513, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "hh:mm:ss";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(900, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 25);
            this.label13.TabIndex = 13;
            this.label13.Text = "Test Id";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(907, 61);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(122, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Generating, Please Wait";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(761, 622);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(101, 53);
            this.button7.TabIndex = 15;
            this.button7.Text = "Save Test (Server)";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(897, 622);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(101, 53);
            this.button8.TabIndex = 16;
            this.button8.Text = "Back & Save (Locally)";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // saveFile
            // 
            this.saveFile.RestoreDirectory = true;
            this.saveFile.ShowHelp = true;
            // 
            // TBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 729);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.author);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.testName);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.timeLimit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.totQuest);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkedListBox1);
            this.Name = "TBuilder";
            this.Text = "Test Builder (Professors & Developers only)";
            this.tabControl1.ResumeLayout(false);
            this.mcq.ResumeLayout(false);
            this.mcq.PerformLayout();
            this.objective.ResumeLayout(false);
            this.objective.PerformLayout();
            this.subjective.ResumeLayout(false);
            this.subjective.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox totQuest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox timeLimit;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label testName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label author;
        private System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.TabPage mcq;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox newOption;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox optionBox;
        private System.Windows.Forms.TextBox question;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage objective;
        private System.Windows.Forms.TabPage subjective;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button remove;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox keywords;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox stepInc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox minMarks;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox totalMarks;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox remarksBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox correctAns;
        public System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.SaveFileDialog saveFile;
    }
}