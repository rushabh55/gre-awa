﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AWA.Questions
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IQuestionAWA" in both code and config file together.
    [ServiceContract]
    public interface IQuestionAWA
    {
        [OperationContract]
        void DoWork();

        [OperationContract]
        Question QuestionGenerate();

        [OperationContract]
        void QuestionSave();
    }

    [DataContract]
    public class Question
    {
        [DataMember]
        public String QuestionText;
        [DataMember]
        public List<String> keywords;
    }
}
