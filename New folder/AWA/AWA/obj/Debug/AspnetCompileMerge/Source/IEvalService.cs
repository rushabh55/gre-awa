﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AWA
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEvalService" in both code and config file together.
    [ServiceContract]
    public interface IEvalService
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        float Evaluate(String text, float maxMarks, float minMarks, float step, int maxWords, List<String> keywords, DateTime timestamp);
    }
}
